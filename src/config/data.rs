use std::path::PathBuf;

use xdg::BaseDirectories;

use crate::config::error::Error as ConfigError;

#[derive(Debug, Default)]
pub struct Config {
    data_dir: Option<PathBuf>,
}

impl Config {
    pub fn new(base_dirs: &BaseDirectories) -> Result<Config, ConfigError> {
        let config_path = base_dirs.place_config_file("config.json")?;

        println!("{:?}", config_path);

        Ok(Default::default())
    }

    pub fn open(_base_dirs: &BaseDirectories) -> Result<Self, ConfigError> {
        unimplemented!()
    }
}
