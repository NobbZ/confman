use std::convert::From;
use std::error;
use std::fmt::{Display, Formatter, Result as FmtResult};
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    IO(IOError),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Error::IO(e) => write!(f, "An IO error occured: {error}", error = e),
        }
    }
}

impl error::Error for Error {}

impl From<IOError> for Error {
    fn from(err: IOError) -> Self {
        Error::IO(err)
    }
}
