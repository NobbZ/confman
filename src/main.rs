mod config;

use std::error::Error;

use xdg::BaseDirectories;

use crate::config::Config;

fn main() -> Result<(), Box<dyn Error>> {
    let base_dirs = BaseDirectories::with_prefix("confman")?;
    let config = Config::new(&base_dirs);

    println!("{:?}", base_dirs);
    println!("{:?}", config);

    Ok(())
}
